package Netland;

our $VERSION = '4.001001';

use Carp;
use strictures 1;

sub import {
  my ($self, @modules) = @_;
  my $pkg = caller;
  my @failed;
  for my $mod (@modules) {
    my $c = "package $pkg; use Netland::$mod";
    eval $c;
    if ($@) { carp $@; push @failed, $mod }
  }
  confess 'Failed to import '.join ' ', @failed if @failed;
  1
}

1;
