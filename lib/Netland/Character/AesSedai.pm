package Netland::Character::AesSedai;
use Carp;
use Moo;
use strictures 1;

use Netland::Types;

use namespace::clean;

extends 'Netland::Character';

has '+guild' => (
  default => sub { 'Aes Sedai' },
);

has '+rank' => (
  isa => sub {
    my $value = $_[0];
    my $valid = nttype_possibles('ranks_for_guild')->get('AesSedai')
      || confess 'Failed to retrieve possible ranks';
    is_Str($value)
    and $valid->has_any(sub { $_ eq $value })
    or confess "$value is not a valid rank for this character class"
  },
);

has ajah => (
  lazy      => 1,
  is        => 'ro',
  isa       => NT_Ajah,
  predicate => 1,
  default   => sub { 'None' },
);


around preserve_keys => sub {
  my ($orig, $self) = @_;
  [
    @{ $self->$orig },
    qw/
      ajah
    /,
  ]
};


1;
