package Netland::Role::JSONify;
use Carp;
use strictures 1;

use Netland 'Error';

use JSON::Tiny 'j';
use Path::Tiny;
use Try::Tiny;

use Role::Tiny;

requires 'TO_JSON';

sub encode_json { j( $_[0] ) }

sub write_json {
  my ($self, $path) = @_;

  confess 'Expected a path' unless $path;

  my $js = $self->encode_json
    || confess 'encode_json returned a false value';

  my $file = path("$path");
  $file->spew( +{ binmode => ':raw' },
    $js
  );

  $path
}

sub new_from_json {
  my ($class, $path) = @_;

  confess 'Expected a path' unless $path;

  my $file = path("$path");
  my $js = $file->slurp_raw;
  confess "new_from_json could not read $file"
    unless $js;

  my $decoded = JSON::Tiny->new->decode($js)
    || confess "Failed to decode JSON from $file";

  ref $decoded eq 'HASH' ? $class->new(%$decoded)
    : ref $decoded eq 'ARRAY' ? $class->new(@$decoded)
    : ref $decoded eq 'SCALAR' ? $class->new($$decoded)
    : confess "Do not know how to handle $decoded"
}

1;
