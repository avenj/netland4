package Netland::Error;
use Moo;
use strictures 1;

extends 'Throwable::Error';

has op => (
  is => 'ro',
  predicate => 'has_op',
);

1;
