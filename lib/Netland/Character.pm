package Netland::Character;
use Carp;
use Moo;
use strictures 1;

use Mojo::Template;

use Netland 'Types';

use namespace::clean;


sub TO_JSON {
  my ($self) = @_;
  +{ (map {; $_ => $self->$_ } @{ $self->preserve_keys }) }
}

sub TO_WIKI {
  my ($self) = @_;

  my $tmpl = sub { local $/; <DATA> }->();
  confess "Missing template in $self (@{[ref $self]})"
    unless $tmpl;

  Mojo::Template->new(
    auto_escape => 0,
  )->render($tmpl, $self)
}

sub preserve_keys {
  ## Keys we ought serialize.
  ## MAKE SURE this is correct.
  ## Subclasses can override to add keys:
  ##   around preserve_keys => sub {
  ##      my ($orig, $self) = @_;
  ##      [
  ##        @{ $self->$orig },
  ##        qw/
  ##          ... more keys ...
  ##        /,
  ##      ]
  ##   };
  [ qw/
      name
      played_by
      forum_url

      points
      biography
      quick_info

      guild
      rank
      status

      bonds
      darkfriend
      position

      height
      weight
      eyes
      hair
      origin

      date_created
      date_raised
      date_updated

  / ]
}



with 'Netland::Role::JSONify';



## Points object.
has points    => (
  lazy     => 1,
  isa      => InstanceOf['Netland::Chars::Points'],
  default  => sub {
    Netland::Points->new
  },
  coerce   => sub {
    unless (ref $_[0] eq 'HASH') {
      confess "Expected a HASH but got $_[0]"
    }
    Netland::Chars::Points->new(%{$_[0]})
  },
);

has biography => (
  required => 1,
  is       => 'ro',
  isa      => Str,
);

has quick_info => (
  required => 1,
  is       => 'ro',
  isa      => Str,
);

## Basic outline.

has name => (
  required => 1,
  is       => 'ro',
  isa      => Str,
);

has played_by => (
  lazy      => 1,
  is        => 'ro',
  isa       => Str,
  default   => sub {
    $self->status eq 'NPC' ? 'NPC' : 'None';
  },
  predicate => 1,
);

has forum_url => (
  lazy     => 1,
  is       => 'ro',
  isa      => Str,
  default  => sub {
    $self->status eq 'NPC' ? 'NPC' : $self->played_by;
  },
  predicate => 1,
);

has guild => (
  lazy     => 1,
  is       => 'ro',
  isa      => NT_Guild,
);

has rank => (
  ## Constrained further by Chars::Class:: classes
  required => 1,
  is       => 'ro',
);

has status => (
  required  => 1,
  is        => 'ro',
  isa       => NT_Status,
  default   => sub { 'Active' },
  predicate => 1,
);

## Extras.

has bonds => (
  lazy      => 1,
  is        => 'ro',
  isa       => ArrayRef,
  default   => sub { [] },
  predicate => 1,
);

has darkfriend => (
  required => 1,
  is       => 'ro',
  isa      => NT_Darkfriend,
);

has position => (
  lazy      => 1,
  is        => 'ro',
  isa       => Str,
  default   => sub { 'None' },
  predicate => 1,
);

## Character stats.

has height => (
  required => 1,
  is       => 'ro',
  isa      => NT_Height,
);

has weight => (
  required => 1,
  is       => 'ro',
  isa      => NT_Weight,
);

has eyes => (
  required => 1,
  is       => 'ro',
  isa      => Str,
);

has hair => (
  required => 1,
  is       => 'ro',
  isa      => Str,
);

has origin => (
  required => 1,
  is       => 'ro',
  isa      => Str,
);

has date_created => (
  required => 1,
  is       => 'ro',
  isa      => Int,
);

has date_raised => (
  ## boolean false if never raised
  is       => 'ro',
  isa      => Int,
  default  => sub { 0 },
);

has date_updated => (
  ## boolean false if never updated
  is       => 'ro',
  isa      => Int,
  default  => sub { 0 },
);




1;

__DATA__
% my ($char) = @_;
====== <%= $char->name %> ======

Played by: <%= $char->played_by %>

===== Character Snapshot =====
^Rank | <%= $char->rank %> |
%
% if ($char->rank eq 'Aes Sedai') {
^Ajah | <%= $char->ajah %> |
% }
%
% if ($char->can('association')) {
^Association | <%= $char->association %> |
% }
%
% if ($char->can('profession')) {
^Profession | <%= $char->profession %> |
% }
%
% if ($char->has_bonds) {
%   for my $thisbond (@{ $char->bonds }) {
^Bonded To | <%= $thisbond %> |
%   } 
% }
%
% if ($char->has_position) {
^Position | <%= $char->position %> |
% }
%
^Status | <%= $char->status %> |
^Darkfriend | <%= $char->darkfriend %> |


^Height | <%= $char->height %> |
^Weight | <%= $char->weight %> |
^Eyes | <%= $char->eyes %> |
^Hair | <%= $char->hair %> |
^Origin | <%= $char->origin %> |
^Date Created | <%= $char->date_created %> |
^Date Raised | <%= $char->date_raised %> |
^Last Update | <%= $char->date_updated %> |


===== Quick Info =====

%= $char->quick_info

===== Points =====

FIXME

===== Biography =====

%= $char->biography
