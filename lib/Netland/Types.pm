package Netland::Types;
use strictures 1;

use MooX::Types::MooseLike;
use MooX::Types::MooseLike::Base ':all';

use List::Objects::WithUtils;

use Scalar::Util qw/
  blessed
  reftype
/;


## These are the array-type possibles:
our %Valid = (
  associations => array(
    'Aiel',
    'Common Man',
    'Merchant',
    'Noble',
    'Ogier',
    'Sea Folk',
    'White Cloak',
  ),

  ajah => array(
    qw/
      None
      Blue
      Brown
      Green
      Grey
      Red
      White
      Yellow
    /,
  ),

  darkfriend => array(
    qw/ 
      Yes 
      No 
      Unknown 
    /
  ),

  guild => array(
    'Aes Sedai',
    'Commoners',
    'Soldiers',
    'Warders',
  ),

  origins => array(
    'Aiel Waste', 'Altara', 'Amadicia',    # A
    'Andor', 'Arad Doman', 'Arafel',       # A
    'Cairhien',                            # C
    'Falme', 'Far Madding',                # F
    'Ghealdan',                            # G
    'Illian', 'Kandor',                    # K
    'Mayene', 'Murandy',                   # M
    'Saldaea', 'Seanchan', 'Shienar',      # S
    'Tar Valon', 'Tarabon', 'Tear',        # T
  ),

  ranks_for_guild => hash(
    AesSedai => array( 'Novice', 'Accepted', 'Aes Sedai' ),
    Commoner => array( 'Apprentice', 'Journeyman', 'Master' ),
    Warder   => array( 'Siswai', 'Manshima', 'Ashanderai', 'Warder' ),
    Soldier  => array( 'Soldier', 'Dedicated', "Asha'man" ), 
  ),

  status => array(
    qw/ 
      Pending
      Rejected
      Active 
      Inactive 
      Dead 
      NPC 
    /
  ),
);

sub nttype_possibles { $Valid{lc $_[0]} }

my $defs = [
  ## Character metadata constraints (NT_$type)

  # Ajah
  {
    name => 'NT_Ajah',
    test => sub {
      my $val = $_[0];
      is_Str($val) 
      and nttype_possibles('ajah')->has_any(sub { $_ eq $val })
    },
    message => sub {
      "$_[0] is not a valid Ajah"
    },
  },

  # Associations
  {
    name => 'NT_Association',
    test => sub {
      my $val = $_[0];
      is_Str($val)
      and nttype_possibles('associations')->has_any(sub { $_ eq $val })
    },
    message => sub {
      "$_[0] is not a valid Association"
    },
  },

  # Darkfriend
  {
    name => 'NT_Darkfriend',
    test => sub {
      my $val = $_[0];
      is_Str($val) 
      and nttype_possibles('darkfriend')->has_any(sub { $_ eq $val })
    },
    message => sub {
      "$_[0] is not a valid Darkfriend boolean value"
    },
  },

  # Guild
  {
    name => 'NT_Guild',
    test => sub {
      my $val = $_[0];
      is_Str($val)
      and nttype_possibles('guild')->has_any(sub { $_ eq $val })
    },
    message => sub {
      "$_[0] is not a valid Netland guild"
    },
  },

  # Height
  {
    name => 'NT_Height',
    test => sub {
      my $val = $_[0];
      $val =~ /^(?:\d+)'(?:\d+)$/ ? 1 
      : $val eq 'NA'    ? 1
      : $val eq 'Ogier' ? 1 
      : ()
    },
    message => sub {
      "$_[0] is not a valid Height"
    },
  },

  # Status
  {
    name => 'NT_Status',
    test => sub {
      my $val = $_[0];
      is_Str($val) 
      and nttype_possibles('status')->has_any(sub { $_ eq $val })
    },
    message => sub {
      "$_[0] is not a valid Status"
    },
  },

  # Weight
  {
    name => 'NT_Weight',
    test => sub {
      my $val = $_[0];
      $val =~ /^(?:\d+)-(?:\d+)$/ ? 1 
      : $val eq 'NA'    ? 1
      : $val eq 'Ogier' ? 1 
      : ()
    },
    message => sub {
      "$_[0] is not a valid Weight"
    },
  },

];

sub nttype_list { map {; $_->{name} } @$defs }

use Exporter 'import';
our @EXPORT_OK = 'nttype_possibles';

MooX::Types::MooseLike::register_types( $defs, __PACKAGE__ );

@EXPORT_OK = array(
  @EXPORT_OK, 
  @MooX::Types::MooseLike::Base::EXPORT_OK
)->uniq->all;
our @EXPORT = @EXPORT_OK;
our %EXPORT_TAGS = (
  all => [ @EXPORT_OK ],
);

1;
